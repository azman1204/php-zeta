<?php
// array ada 2 jenis : 1. normal array, 2. associative array

$names = ['Abu', 'Ali'];
$names[] = 'Ah Chong';
echo "Name postion 3 " . $names[2]; 
// note : index start from 0


// associative array - key => value

$person = [
    'name' => 'Abu',
    'address' => 'Bangi',
    'age' => 40
];

echo "NAME = " . $person['name'];
echo "Name = {$person['name']}, Address = {$person['address']}, age = {$person['age']}";








