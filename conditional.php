<?php
$umur = 61;

if ($umur <= 6) {
    print("baby");
} else if ($umur > 6 && $umur <= 18) {
    echo "Teenager";
} else if ($umur > 18 && $umur <= 60) {
    print('Adult');
} else {
    print('Old');
}
// Note : && -> AND, || -> OR, ! -> NOT, 
// == -> Equal, === -> Equal

// Switch ... case
const WARNA = 'x'; // x boleh ubah value nya
echo WARNA;
switch (WARNA) {
    case 'b':
        echo 'Blue <br>';
        break;
    case 'g':
        echo 'Green <br>';
        break;
    default:
        echo 'Unknown <br>';
}

// ternary
$suhu = 45;
$cuaca = $suhu > 35 ? 'Panas' : 'Biasa';
echo "Cuaca = $cuaca";

// ini shortcut kpd
if ($suhu > 35) {
    return 'Panas';
} else {
    return 'Biasa';
}