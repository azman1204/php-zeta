<?php
// gitlab.com/azman1204
// while loop
$no = 0;
while ($no <= 10) {
    $no++;

    if ($no == 5) {
        continue; // do not run the following stmt
    }

    if ($no == 9) {
        break; // exit loop
    }

    echo "NO = $no <br>";
}

// do..while loop
$bil = 1;
do {
    echo "BIL = " . $bil . '<br>';
    $bil++;
} while ($bil <= 10);

// for loop
for ($i = 1; $i <= 10; $i++) {
    echo "I = $i <br>";
}

// foreach loop - loop khas utk array
$names = ['Abu', 'Ali', 'Ahmad', 'Ah Kow', 'Muthu'];
foreach ($names as $n) {
    echo "Name = $n <br>";
}